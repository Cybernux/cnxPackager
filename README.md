cnxPackager
===========

is to be a set of scripts designed to (semi-)automate building the packages
we plan to use within the Cybernux OS distribution. 

These include but are not limited to rebranding, fixes, changes, modds, additons, 3rd party software., etc.

We will keep most of those packages within a git repository, such as this one.


### What is planned?

  * chrooting for the build
  * write a script that pulls the software from git
  * build and publish a debian like package along with sources
  * a script to publish on internal servers for testing
  * a script to push the internal repository to our public repository.
  
### What tools will be used

I'm thinking of keeping things simple. We don't need to pull ALL of the
packages we need, since most of them are already on a Debian or Ubuntu archive.

So some some simple bash scripting along with _reprepro_ should be enough.


### Documentation

Very important. We **will** get lost without that, so a [_wiki_](wikis/home) will be imlemented here.


### Want to help out?

Great! Just [email me](mailto:hyperclock@cybernux.eu) and let me know.
